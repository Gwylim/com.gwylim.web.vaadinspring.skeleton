package com.gwylim.skeleton.vaadin;

import com.gwylim.skeleton.vaadin.service.Service;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.beans.factory.annotation.Autowired;

@UIScope
@Route("")
@Theme(value = Lumo.class)
@HtmlImport("frontend:///styles/styles.html")
@HtmlImport("frontend://bower_components/shadycss/apply-shim.html")
@HtmlImport("frontend://bower_components/shadycss/custom-style-interface.html")
public class RootView extends VerticalLayout {

    private final Label label;

    @Autowired
    public RootView(final Service service) {
        add(new Label("Hello!"));

        label = new Label("Count: " + service.getCount());
        add(label);

        add(new Button("Count++",
                       event -> label.setText("Count: " + service.getAndIncrementCount())
        ));

        add(new RouterLink("Push View", PushView.class));

        add(new Image("frontend/img/shed.png", "Shed"));
    }
}
