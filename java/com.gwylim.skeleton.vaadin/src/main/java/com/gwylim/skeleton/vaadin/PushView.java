package com.gwylim.skeleton.vaadin;

import com.gwylim.skeleton.vaadin.service.Service;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Example of push updates...
 */
@Push
@Route("push")
@Theme(Lumo.class)
@HtmlImport("frontend:///styles/styles.html")
@StyleSheet("frontend:///styles/extraStyle.css")
public class PushView extends VerticalLayout {
    private static final Logger LOG = LoggerFactory.getLogger(PushView.class);
    public static final int UPDATE_FREQUENCY_MS = 1000;
    private final Service service;

    private final Span countLabel;

    private FeederThread thread;

    public PushView(final Service service) {
        this.service = service;
        this.countLabel = new Span("Waiting for updates");
        this.countLabel.setClassName("count");
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        add(countLabel);

        // Start the data feed thread
        thread = new FeederThread(attachEvent.getUI(), this);
        thread.start();
    }

    @Override
    protected void onDetach(final DetachEvent detachEvent) {
        LOG.info("OnDetach called.");
        // Cleanup
        thread.interrupt();
        thread = null;
    }

    private static class FeederThread extends Thread {
        private final UI ui;
        private final PushView view;

        FeederThread(final UI ui, final PushView view) {
            this.ui = ui;
            this.view = view;
        }

        @Override
        public void run() {
            try {
                // Update the data for a while
                // noinspection InfiniteLoopStatement
                while (true) {
                    // Sleep to emulate background work
                    Thread.sleep(UPDATE_FREQUENCY_MS);
                    final String message = "Sending count=" + view.service.getCount();

                    ui.access(() -> view.countLabel.setText(message));
                    LOG.info(message);
                }
            }
            catch (final InterruptedException e) {
                LOG.info("Update thread terminated.");
            }
        }
    }
}
