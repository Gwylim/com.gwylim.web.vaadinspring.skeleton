package com.gwylim.skeleton.vaadin.service;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Service {
    public static final AtomicLong COUNTER = new AtomicLong(0);

    public long getAndIncrementCount() {
        return COUNTER.incrementAndGet();
    }

    public long getCount() {
        return COUNTER.get();
    }
}
